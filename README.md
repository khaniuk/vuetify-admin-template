# Vuetify Admin Template

Frontend - Admin template with Vue + Vuex + Vuetify

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Login data
```
username: admin
password: admin
```
