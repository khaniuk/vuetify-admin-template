import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
  },
  mutations: {
    authSuccess(state,token) {
      state.token = token;
      state.status = 'success';
    },
    authError(state) {
      state.token = '';
      state.status = 'error';
    },
    authLogout(state) {
      state.token = '';
      state.status = '';
    },
  },
  actions: {
    login({ commit }, ldata) {
      return new Promise((resolve, reject) => {
        // Inicio de simulacion de login
        if (ldata.email == 'admin' && ldata.password == 'admin') {
          let token = 'Bearer dabsdkadasdkajdahdgakdaskdjadyada';
          localStorage.setItem('token', token);
          commit('authSuccess', token);
          resolve({status: 'success', msg: ''});
        } else {
          commit('authError', 'Error en autenticación');
          reject({status: 'error', msg: 'Usuario o Contraseña incorrecto'});
        }
        // Fin de simulacion de login

        // Inicio de login con backend
        /* axios.post('/login', ldata)
          .then((response) => {
            let token = response.data.auth.access_token;
            localStorage.setItem('token', token);
            commit('authSuccess', token);
            axios.defaults.headers.common['Authorization'] = "Bearer " + accessToken;
            resolve({status: 'success', msg: ''});
          })
          .catch((error) => {
            localStorage.removeItem('token');
            commit('authError', 'Error en autenticación');
            reject({status: 'error', msg: 'Usuario o Contraseña incorrecto'});
          }) */
      })
    },
    logout({ commit }) {
      // eslint-disable-next-line
      return new Promise((resolve, reject)=>{
        commit('authLogout');
        localStorage.removeItem('token');
        // delete axios.defaults.headers.common['Authorization'];
        resolve();
      })
    },
  },
  getters: {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status,
  }
});
