export default {
  getServe() {
    return "http://localhost:3000/"
  },
  getBase() {
    return this.getServe() + "api/";
  },
  getToken() {
    return "Bearer " + localStorage.getItem("token");
  },
  getHeader() {
    return {
      headers: {
        Accept: "application/json",
        Authorization: this.getToken()
      }
    };
  }
};
